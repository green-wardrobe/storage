FROM openjdk:11.0-jdk
ENV TZ "Asia/Ho_Chi_Minh"
RUN rm -rf /var/cache/apk/*
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /usr/app/
WORKDIR /usr/app
EXPOSE 8081
ENTRYPOINT exec java $JAVA_OPTS -jar $JAVA_ARGS springboot-file-upload-download-api-0.0.1-SNAPSHOT.jar