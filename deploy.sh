#!/bin/bash
ssh mhsolution@192.168.1.83 'docker pull docker-daemon:5555/alert-tc2-cdn:v1.1.'$1
ssh mhsolution@192.168.1.83 'docker rm -f alert-tc2-cdn'
ssh mhsolution@192.168.1.83 'docker run --name alert-tc2-cdn -v /etc/hosts:/etc/hosts:ro -v /home/mhsolution/alert-tc2/cdn/upload/image:/usr/app/upload/image -d -p 8121:8081 --restart unless-stopped docker-daemon:5555/alert-tc2-cdn:v1.1.'$1