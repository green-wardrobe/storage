#!/bin/bash
mvn clean install
docker build -t qr-code-cdn:v1.1.1 .
docker pull qr-code-cdn:v1.1.1
docker rm -f qr-code-cdn
docker run --name qr-code-cdn -v /etc/hosts:/etc/hosts:ro -v /root/qr/upload/image:/usr/app/upload/image -d -p 8121:8081 --restart unless-stopped qr-code-cdn:v1.1.1
